import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService) { }

    public likePost(post: Post, currentUser: User) {
        debugger;
        const innerPost = post;
        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: true,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        let isLike: boolean;
        if (hasReaction) {
            isLike = innerPost.reactions.filter((x) => x.user.id === currentUser.id)[0].isLike;
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
        }
        if (!hasReaction || isLike !== reaction.isLike) {
            innerPost.reactions = innerPost.reactions.concat({ isLike: true, user: currentUser });
        }
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                let isLike: boolean;
                if (hasReaction) {
                    isLike = innerPost.reactions.filter((x) => x.user.id === currentUser.id)[0].isLike;
                    innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
                }
                if (!hasReaction || isLike !== reaction.isLike) {
                    innerPost.reactions = innerPost.reactions.concat({ isLike: true, user: currentUser });
                }
                return of(innerPost);
            })
        );
    }

    public dislikePost(post: Post, currentUser: User) {
        debugger;
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: false,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        let isLike: boolean;
        if (hasReaction) {
            isLike = innerPost.reactions.filter((x) => x.user.id === currentUser.id)[0].isLike;
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
        }
        if (!hasReaction || isLike !== reaction.isLike) {
            innerPost.reactions = innerPost.reactions.concat({ isLike: false, user: currentUser });
        }
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                let isLike: boolean;
                if (hasReaction) {
                    isLike = innerPost.reactions.filter((x) => x.user.id === currentUser.id)[0].isLike;
                    innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
                }
                if (!hasReaction || isLike !== reaction.isLike) {
                    innerPost.reactions = innerPost.reactions.concat({ isLike: false, user: currentUser });
                }
                return of(innerPost);
            })
        );
    }
}
