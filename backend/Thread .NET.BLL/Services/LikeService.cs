﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        public LikeService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if (likes.Any())
            {
                var isLike = likes.First().IsLike;
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                if (isLike == reaction.IsLike)
                {
                    return;
                }

            }
            _context.PostReactions.Add(_mapper.Map<PostReaction>(reaction));

            await _context.SaveChangesAsync();
        }
    }
}
