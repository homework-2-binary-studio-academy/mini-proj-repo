﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.MappingProfiles
{
    public class PostReactionProfile : Profile
    {
        public PostReactionProfile()
        {
            CreateMap<NewReactionDTO, PostReaction>()
                .ForMember(dest => dest.PostId, src => src.MapFrom(post => post.EntityId));

        }
    }
}
